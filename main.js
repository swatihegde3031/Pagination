// DOM elements
const resultsContainer = document.getElementById('results-container');
const loadingIndicator = document.getElementById('loading-indicator');

// API configuration
const API_BASE_URL = 'https://jsonplaceholder.typicode.com';
const ITEMS_PER_PAGE = 10;
let currentPage = 1;

// Function to fetch and display results
async function fetchAndDisplayResults() {
    try {
        const response = await fetch(`${API_BASE_URL}/posts?_page=${currentPage}&_limit=${ITEMS_PER_PAGE}`);
        if (!response.ok) {
            throw new Error(`Failed to fetch data. Status: ${response.status}`);
        }

        const data = await response.json();

        if (data.length > 0) {
            data.forEach(item => {
                const resultItem = document.createElement('div');
                resultItem.classList.add('result-item');
                resultItem.innerHTML = `
                    <h2>${item.title}</h2>
                    <p>${item.body}</p>
                `;
                resultsContainer.appendChild(resultItem);
            });
            currentPage++;
        } else {
            loadingIndicator.textContent = 'No more results';
            intersectionObserver.unobserve(loadingIndicator);
        }
    } catch (error) {
        console.error('Error fetching data:', error);
    }
}

// Intersection Observer to trigger fetching
const intersectionObserver = new IntersectionObserver((entries) => {
    if (entries[0].isIntersecting) {
        fetchAndDisplayResults();
    }
});

intersectionObserver.observe(loadingIndicator);

// Initial load
fetchAndDisplayResults();
